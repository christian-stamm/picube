#include <math.h>
#include <stdio.h>
#include "pico/stdlib.h"

int main()
{
    const uint led_pin = 25;

    // Initialize LED pin
    gpio_init(led_pin);
    gpio_set_dir(led_pin, GPIO_OUT);

    // Initialize chosen serial port
    stdio_init_all();

    // Loop forever

    while (true)
    {
        for (uint ms = 0; ms < 1000; ms += 100)
        {
            gpio_put(led_pin, true);
            sleep_ms(ms);
            gpio_put(led_pin, false);
            sleep_ms(ms);
        }

        for (uint ms = 1000; 0 < ms; ms -= 100)
        {
            gpio_put(led_pin, true);
            sleep_ms(ms);
            gpio_put(led_pin, false);
            sleep_ms(ms);
        }
    }
}