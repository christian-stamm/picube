#include <memory>
#include "pixelloader.hpp"
#include "hardware/gpio.h"

PixelLoader::PixelLoader()
    : enabled(false)
    , spiID(spi_default)
{
    enable();
}

void PixelLoader::enable()
{
    enabled = true;
    spi_init(spi0, 1e6);
    spi_set_slave(spi0, true);
    spi_set_format(spi0, 8, SPI_CPOL_1, SPI_CPHA_1, SPI_MSB_FIRST);
    gpio_set_function(PICO_DEFAULT_SPI_RX_PIN, GPIO_FUNC_SPI);
    gpio_set_function(PICO_DEFAULT_SPI_SCK_PIN, GPIO_FUNC_SPI);
    gpio_set_function(PICO_DEFAULT_SPI_TX_PIN, GPIO_FUNC_SPI);
    gpio_set_function(PICO_DEFAULT_SPI_CSN_PIN, GPIO_FUNC_SPI);
}



void PixelLoader::load(PixelBuffer &target) const
{
    const uint16_t transmitSize = 10;
    uint32_t sumErrors = 0;

    uint8_t tx_buffer[transmitSize];
    uint8_t rx_buffer[transmitSize];

    for(int i = 0; i < transmitSize; i++)
    {
        tx_buffer[i] = (uint8_t)(255 - i);
    }

    spi_write_read_blocking(spi0, tx_buffer, rx_buffer, transmitSize);

    for(int it = 0; it < 1e2; it++)
    {
        uint8_t size = spi_write_read_blocking(spi0, tx_buffer, rx_buffer, transmitSize);
    
        // printf("Start packet check.\n");

        for(int i = 0; i < transmitSize; i++)
        {
            uint8_t expectedValue = i;
            uint8_t receivedValue = rx_buffer[i];

            bool isValid = (expectedValue == receivedValue);
            // printf("Packet ID %i | Valid=%i | Expected=%i | Received=%i\n", i, isValid, expectedValue, receivedValue);

            if(!isValid)
            {
                sumErrors += 1;
            }
        }

        // printf("Packet check finished.\n");
        // printf("Until loop %i a total of %i errors occured.\n", it+2, sumErrors);
    }

    printf("Until loop %i a total of %i errors occured.\n", 100, sumErrors);
}

void PixelLoader::disable()
{
    enabled = false;
}

bool PixelLoader::isEnabled() const
{
    return enabled;
}