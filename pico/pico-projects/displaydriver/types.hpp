#pragma once

#include <memory>
#include <Eigen/Dense>

class Pixel;

using Color = Eigen::Vector<uint16_t, 3>;
using Position = Eigen::Vector<uint8_t, 3>;
using PioBuffer = std::vector<uint8_t>;
using PixelBuffer = std::vector<Pixel>;
using ColorBuffer = std::vector<Color>;