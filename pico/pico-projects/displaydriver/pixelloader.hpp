#pragma once

#include "types.hpp"
#include "hardware/spi.h"

class PixelLoader
{
    
public:

    PixelLoader();

    void enable();
    void disable();
    bool isEnabled() const;

    void load(PixelBuffer &target) const;

private:

    bool enabled;
    spi_inst_t *spiID;
};