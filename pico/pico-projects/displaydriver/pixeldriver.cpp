#include <cmath>
#include <bitset>
#include <sstream> 
#include <functional>

#include "pico/types.h"
#include "pico/time.h"
#include "pico/sync.h"
#include "pico/stdlib.h"
#include "hardware/clocks.h"
#include "hardware/dma.h"
#include "hardware/irq.h"

#include "pixeldriver.hpp"
#include "pixeldriver.pio.h"

#define DMA_CHANNEL 0
#define DMA_CHANNEL_MASK (1u << DMA_CHANNEL)

mutex_t xferLock;
uint ditherIndex = 0;

int64_t __isr reset_delay_complete(alarm_id_t id, void *user_data) {
    mutex_exit(&xferLock);
    return 0;
}

void __isr dma_complete_handler()
{
    if (dma_hw->ints0 & DMA_CHANNEL_MASK) 
    {
        dma_hw->ints0 = DMA_CHANNEL_MASK;
        // when the dma is complete we start the reset delay timer
        add_alarm_in_us(280, reset_delay_complete, nullptr, true);
    }
}

PixelDriver::PixelDriver()
    : pioID(pio0)
    , pioSM(0)
    , pioMEM(0)
    , pioBuffer(PioBuffer(12288 * 16))
    // , pixelBuffer(PixelBuffer(4096))
    // , colorBuffer(ColorBuffer(4096))
    , updateFrequency(800e3)
    , ledsPerChannel(32)
    , databitsPerLED(24)
    , channelsPerShiftRegister(8)
    , numChainedShiftRegisters(2)
    , numPioChannels(8)
    , pinDIN{4,5,6,7,8,9,10,11}
    , pinSH(12)
    , pinST(13)
    , pinRST(14)
    , pinOE(15)
{
    mutex_init(&xferLock);
    this->load();
}

void PixelDriver::load()
{   
    gpio_init(pinRST);
    gpio_init(pinOE);

    gpio_set_dir(pinRST, GPIO_OUT);
    gpio_set_dir(pinOE, GPIO_OUT);

    initPio();
    initDMA();

    // Enable Shift Register
    gpio_put(pinRST, 1);
    gpio_put(pinOE, 0);

    mutex_exit(&xferLock);
}

void PixelDriver::initPio()
{
    std::vector<uint> pins(
        {
            pinDIN[0],
            pinDIN[1], 
            pinDIN[2],
            pinDIN[3],
            pinDIN[4],
            pinDIN[5],
            pinDIN[6],
            pinDIN[7],
            pinSH,
            pinST,
        }
    );

    uint pinBase = pins.front();
    uint pinCount = pins.size();

    pioSM = pio_claim_unused_sm(pioID, false);

    // Init Pins
    for(uint pin = 0; pin < pinCount; pin++)
    {
        pio_gpio_init(pioID, pins[pin]);
    }

    // Set Pins to output direction
    pio_sm_set_consecutive_pindirs(pioID, pioSM, pinBase, pinCount, true);

    // Find free location in PIO's instruction memory
    pioMEM = pio_add_program(pioID, &displaydriver_program);
    pio_sm_config smConfig = displaydriver_program_get_default_config(pioMEM);

    sm_config_set_out_pins(&smConfig, pinBase, pinCount);
    sm_config_set_sideset_pins(&smConfig, pinSH);

    // Shift to right with threshold
    sm_config_set_out_shift(&smConfig, true, true, 8);
    sm_config_set_fifo_join(&smConfig, pio_fifo_join::PIO_FIFO_JOIN_TX);

    // Set clockdivider
    uint32_t sysCLK = clock_get_hz(clk_sys);
    uint32_t numOps = (2 + 2*16) + (2 + 2*16) + (2 + 3*16 + 11);
    float clkDiv = (1.0 * sysCLK) / (1.0 * numOps * updateFrequency);

    assert(clkDiv < (1 << 16));
    sm_config_set_clkdiv(&smConfig, clkDiv);

    // Load our configuration, and jump to the start of the program
    pio_sm_init(pioID, pioSM, pioMEM, &smConfig);

    // Set the state machine running
    pio_sm_set_enabled(pioID, pioSM, true);
}

void PixelDriver::initDMA()
{
    dma_claim_mask(DMA_CHANNEL_MASK);
    dma_channel_config dmaConfig = dma_channel_get_default_config(DMA_CHANNEL);
    channel_config_set_dreq(&dmaConfig, pio_get_dreq(pioID, pioSM, true));
    channel_config_set_read_increment(&dmaConfig, true);                       
    channel_config_set_write_increment(&dmaConfig, false);
    channel_config_set_transfer_data_size(&dmaConfig, DMA_SIZE_8);
    dma_channel_configure(DMA_CHANNEL, &dmaConfig, &pioID->txf[pioSM], nullptr, 0, false);
    irq_set_exclusive_handler(DMA_IRQ_0, dma_complete_handler);
    dma_channel_set_irq0_enabled(DMA_CHANNEL, true);
    irq_set_enabled(DMA_IRQ_0, true);
}

void PixelDriver::unload()
{
    pio_sm_set_enabled(pioID, pioSM, false);
    pio_remove_program(pioID, &displaydriver_program, pioMEM);
    pio_sm_unclaim(pioID, pioSM);

    // Disable Shift Register
    gpio_put(pinRST, 0);
    gpio_put(pinOE, 1);

    mutex_exit(&xferLock);
}

void PixelDriver::updatePio()
{
    mutex_enter_blocking(&xferLock);
    dma_channel_transfer_from_buffer_now(DMA_CHANNEL, &pioBuffer.at(ditherIndex * 12288), 12288);
    
    ditherIndex += 1;
    ditherIndex %= 16;
}

void PixelDriver::updatePixels(PixelBuffer &pixels)
{    
    Color color;
    
    // absolute_time_t startTime = get_absolute_time();

    for(Pixel &pixel : pixels)
    {
        uint16_t ledIndex = pixel.getIndex();
        uint8_t pioChannel = (uint8_t)((uint16_t)(ledIndex / 32.0) % 8);
        uint16_t baseAddress = ((uint16_t)(ledIndex / 256)) * 384 + ((uint16_t)(ledIndex / 16) % 2) * 6144;
        uint8_t shiftRegOffset = (uint8_t)(ledIndex % 16);
        
        for(int ditherBit = 0; ditherBit < 16; ditherBit++)
        {
            uint32_t ditherOffset = 12288 * ditherBit;
            pixel.dither(color);
        
            for(int rgb = 0; rgb < 3; rgb++)
            {
                for(int bit = 0; bit < 8; bit++)
                {
                    uint16_t bitOffset = (16 * (bit + 8 * rgb));
                    uint32_t bufferAddress = baseAddress + shiftRegOffset + bitOffset + ditherOffset;
                    uint8_t colorMask = 0b10000000 >> bit;
                    bool isBitSet = (color[rgb] & colorMask) != 0;

                    bufferAddress = std::min<uint32_t>(bufferAddress, 16 * 12288);

                    pioBuffer[bufferAddress] &= ~(1 << pioChannel);
                    pioBuffer[bufferAddress] |= (uint8_t)(isBitSet << pioChannel);
                }
            }
        }
    }

    // int64_t dt = absolute_time_diff_us(startTime, get_absolute_time());
    // printf("DT=%ld\n", (long)dt);
}