#pragma once

#include "types.hpp"
#include "pixel.hpp"

#include "pico/types.h"
#include "hardware/pio.h"
#include "hardware/irq.h"

class PixelDriver
{
public:

    PixelDriver();

    void load();
    void unload();
    void updatePio();
    void updatePixels(PixelBuffer &pixel);

private:

    PIO pioID;
    uint pioSM;
    uint pioMEM;
    
    PioBuffer pioBuffer;
    // PixelBuffer pixelBuffer;
    // ColorBuffer colorBuffer;
    
    const uint updateFrequency;
    const uint ledsPerChannel;
    const uint databitsPerLED;
    const uint channelsPerShiftRegister;
    const uint numChainedShiftRegisters;
    const uint numPioChannels;

    const uint pinDIN[8];
    const uint pinSH;
    const uint pinST;
    const uint pinRST;
    const uint pinOE;

    void initPio();
    void initDMA();
};