# Installation Gideline
The installation process is divided into three parts.
It was written for the Operating System (OS) `Ubuntu Server 22.04 LTS (64-bit)`.


```
git clone https://gitlab.com/christian-stamm/picube-dev.git
cd picube/install
chmod +x setup*
./online_setup.sh
./remote_setup.sh
```

# Preparation
The following hardware is required to setup the project:
- Workstation to remote access the pi via ssh [@Workstation]
- Raspberry Pi 4 to execute the picube software [@Raspberry]

The following software needs to be installed [@Workstation]
    - [Raspberry Pi Imager](https://www.raspberrypi.com/software/) to flash a Operating System (OS) on a SD-Card.
    - [VSCode](https://code.visualstudio.com/Download) as IDE for software developement. 

### Preparation on your local remote PC
- Open [Raspberry Pi Imager](https://www.raspberrypi.com/software/) an set the following configuration:
    - Operating System: Ubuntu 22.04 LTS 
    - Set `picube` as hostname
    - Set `devel` as user 
    - Add a wifi network
- Write image on a SD-Card
- Download this [repository](https://gitlab.com/christian-stamm/picube/-/archive/master/picube-master.tar.gz) as `tar.gz` and copy the archive on a USB drive.

```

### Online installation on your Raspberry Pi:
- Plug the USB drive into the Pi.
- Find USB Device on Raspberry Pi: `sudo blkid` (i.e. "dev/sda1")
- Create a mount directory: `sudo mkdir -p /media/usb/`
- Mount the detected USB Drive: `sudo mount /dev/sd[XX] /media/usb` 
- Navigate to your Home directory: `cd`
- Copy the archive to your Home directory: `sudo cp /media/usb/picube-master.tar.gz $HOME`
- Extract the archive: `tar -zxf picube-master.tar.gz`
- Delete archive: `rm picube-master`
- Rename folder: `mv picube-master picube`
- Navigate to the installation subdirectory: `cd picube/pi/install/`
- Set both install scripts as executable: `chmod +x [prefix]_setup.sh`
- Run the `online_setup.sh` script on the pi: `./online_setup.sh`

The `online_setup.sh` will automatically:
- Set your Raspberry Pi's IP address to: "192.168.178.32" (Default Hostname="picube")
- Generate a SSH key
- Allow yout to use SSH root login

### Remote installation from your external local PC (via SSH):
- Copy the remote SSH Key to the pi: `ssh-copy-id devel@picube`
- Connect via SSH to the pi: `ssh devel@picube`
- Navigate to the installation subdirectory: `cd $HOME/picube/pi/install/`
- Run the shell script: `./remote_setup.sh`
- During the installation your public SSH Key of your Raspbbery Pi is displayed. This is just for maintenance.

The `remote_setup.sh` will automatically:
- Install most dependencies for developing
- Install this repo on the pi
- Install all necessary PATH Variables
- Build all sofware tools to program and debug the raspberry pico

### Environment setup on remote PC:
For software developement [VSCode](https://code.visualstudio.com/Download) is used.\
Install VSCode on your remote PC and and connect in VSCode via SSH to the raspberry pi.
- Install the following VSCode Extensions on the Pi:
    - C/C++
    - C/C++ Extensions Pack
    - CMake
    - CMake Tools
    - Cortex-Debug
    - GitLens

Congratulations the installation is done!
